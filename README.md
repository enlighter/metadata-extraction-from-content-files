# metadata-extraction-from-content-files
Extract metadata automatically from educative content files (ebooks, articles, etc.)

Currently supports filetype:
- pdf
- epub

# This repository contains submodules:
To get the complete repository alongwith submodule, clone recursively, i.e.,
- git clone --recursive https://github.com/enlighter/metadata-extraction-from-content-files.git
